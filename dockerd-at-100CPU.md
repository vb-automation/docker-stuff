As per https://forums.docker.com/t/dockerd-using-100-cpu/94962/5

If a docker host runs out of space, docker logs could get corrupt, which results into high CPU usage by dockerd. (Consequently resulting into pour performance of the running containers.)
In order to resolve the problem, it should be sufficient to remove the corrupted log files.

Corrupt log files can be identified by:

<code>
find /var/lib/docker/containers/ -name *-json.log -exec bash -c 'jq '.' {} > /dev/null 2>&1 || echo "file corrupt: {}"' \; 
</code>


Example run:

Initial top

<code>
top - 12:11:00 up 1 day, 22:42,  1 user,  load average: 1,34, 1,69, 1,65
Tasks: 170 total,   1 running, 169 sleeping,   0 stopped,   0 zombie
%Cpu(s): 39,1 us,  0,7 sy,  0,0 ni, 60,3 id,  0,0 wa,  0,0 hi,  0,0 si,  0,0 st
MiB Mem :    922,0 total,     29,3 free,    364,1 used,    528,5 buff/cache
MiB Swap:    100,0 total,      0,0 free,    100,0 used.    515,5 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                                                            
27084 root      20   0  916332  11036    604 S 100,7   1,2 133:10.43 dockerd                                                                                            
 1828 root      20   0  959392  89092   6856 S  49,3   9,4 319:05.00 influxd                                                                                            
 2351 pi        20   0  229760  75300  14492 S   3,3   8,0  41:26.02 node-red                                                                                           
30949 pi        20   0   13904   3216   2760 R   1,3   0,3   0:00.22 top                                                                                                
 1668 root      20   0  803160   2144    804 S   1,0   0,2   1:12.80 containerd-shim                                                                                    
 1671 root      20   0  803480   1608    444 S   1,0   0,2   1:17.18 containerd-shim                                                                                    
 1809 472       20   0  886096  44012  23044 S   0,7   4,7  11:48.44 grafana-server                                                                                     
   14 root      20   0       0      0      0 I   0,3   0,0   2:31.99 rcu_sched                                                                                          
 1670 root      20   0  803160   1604    512 S   0,3   0,2   2:09.92 containerd-shim                                                                                    
 1672 root      20   0  803160   1096      0 S   0,3   0,1   1:15.00 containerd-shim                                                                                    
 16
</code>

Corupt log files identification and removal:

<code>
sudo  find /media/ext-hdd/docker/containers  -name *-json.log -exec bash -c 'jq '.' {} > /dev/null 2>&1 || echo "file corrupt: {}"' \;
file corrupt: /media/ext-hdd/docker/containers/901f8b99f3289171f2590a674d0ad3b3a2d7eaa6118b704eb93fc743dd54d003/901f8b99f3289171f2590a674d0ad3b3a2d7eaa6118b704eb93fc743dd54d003-json.log

rm -rf /media/ext-hdd/docker/containers/901f8b99f3289171f2590a674d0ad3b3a2d7eaa6118b704eb93fc743dd54d003/901f8b99f3289171f2590a674d0ad3b3a2d7eaa6118b704eb93fc743dd54d003-json.log
</code>

Post-delete top kept showing dockerd at 100CPU.
Bouncing docker <code>sudo systemctl restart docker</code> seems to have fixed the problem.
