#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="chronograf"

CHRONOGRAF_DIR=${EXTHDD_MOUNTPOINT}/chronograf
mkdir -p ${CHRONOGRAF_DIR}/data

# stop/kill the current container
docker rm -f $(docker ps -a | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container

#docker run -d --restart=always -p 8088:8088 --name ${IMAGE_NAME} -v ${CHRONOGRAF_DIR}/data:/chronografdata ${DOCKER_TZ} ${DOCKER_NETWORK} ${DOCKER_HOST} --net-alias rasbr-${IMAGE_NAME} mendhak/arm32v6-chronograf:latest --influxdb-url=http://influxdb:8086 --bolt-path /chronografdata/bolt.db 


docker run -d --restart=always -p 8088:8088 --name ${IMAGE_NAME} -v ${CHRONOGRAF_DIR}/data:/chronografdata ${DOCKER_TZ} ${DOCKER_NETWORK} ${DOCKER_HOST} --net-alias rasbr-${IMAGE_NAME} mendhak/arm32v6-chronograf:latest --influxdb-url=http://rasbr-bridge:8086 --bolt-path /chronografdata/bolt.db 
