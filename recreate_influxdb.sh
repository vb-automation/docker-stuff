#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="influxdb"

INFLUX_DIR=${EXTHDD_MOUNTPOINT}/influx

# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container

docker run -d --restart=always -p 8086:8086 --name ${IMAGE_NAME} -v ${INFLUX_DIR}/data:/var/lib/influxdb -v ${INFLUX_DIR}/config:/etc/influxdb ${DOCKER_TZ} ${DOCKER_NETWORK} ${DOCKER_HOST} --net-alias rasbr-${IMAGE_NAME} arm32v7/influxdb:latest




