#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="grafana"

#NODERED_DATA=${EXTHDD_MOUNTPOINT}/nodered
GRAFANA_DIR=${EXTHDD_MOUNTPOINT}/grafana

mkdir -p ${GRAFANA_DIR}/data ${GRAFANA_DIR}/provisioning
chown 472:472 ${GRAFANA_DIR}/data

# initialiaze configuration
#docker run --rm --entrypoint /bin/bash grafana/grafana:latest -c 'cat $GF_PATHS_CONFIG' > ${GRAFANA_DIR}/grafana.ini

# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container
sudo docker run -d --restart=always -p 3000:3000 -v ${GRAFANA_DIR}/data:/var/lib/grafana -v ${GRAFANA_DIR}/provisioning:/etc/grafana/provisioning -v ${GRAFANA_DIR}/grafana.ini:/etc/grafana/grafana.ini ${DOCKER_TZ} --name ${IMAGE_NAME} ${DOCKER_NETWORK} --net-alias rasbr-${IMAGE_NAME} ${DOCKER_HOST} grafana/grafana:latest

# Run the container
#sudo docker run -d --restart=always -p 1880:1880 -v ${NODERED_DATA}:/data ${DOCKER_TZ} --name ${IMAGE_NAME} ${DOCKER_NETWORK} --net-alias rasbr-${IMAGE_NAME} ${DOCKER_HOST} nodered/node-red:latest

