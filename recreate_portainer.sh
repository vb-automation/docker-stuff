#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="portainer"
PORTAINER_DATA=${EXTHDD_MOUNTPOINT}/${IMAGE_NAME}

mkdir -p ${PORTAINER_DATA}

# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container
sudo docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v ${PORTAINER_DATA}:/data portainer/portainer-ce:latest

#sudo docker run -d --restart=always -p 1880:1880 -v ${NODERED_DATA}:/data ${DOCKER_TZ} --name ${IMAGE_NAME} ${DOCKER_NETWORK} --net-alias rasbr-${IMAGE_NAME} ${DOCKER_HOST} nodered/node-red:latest

