#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="node-red"

NODERED_DATA=${EXTHDD_MOUNTPOINT}/nodered

# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container
sudo docker run -d --restart=always -p 1880:1880 -v ${NODERED_DATA}:/data ${DOCKER_TZ} --name ${IMAGE_NAME} ${DOCKER_NETWORK} --net-alias rasbr-${IMAGE_NAME} ${DOCKER_HOST} nodered/node-red:latest

