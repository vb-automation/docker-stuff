#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="mosquitto"


# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container

docker run -d --restart=always -p 1883:1883 -p 9001:9001 ${DOCKER_TZ} ${DOCKER_HOST} ${DOCKER_NETWORK} --net-alias rasbr-${IMAGE_NAME} --name ${IMAGE_NAME} raspberryvalley/mosquitto:rpi

