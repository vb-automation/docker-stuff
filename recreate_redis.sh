#!/bin/bash
# Include env variables
. docker.env

# Setup ENV variables
IMAGE_NAME="redis"

REDIS_DATA=${EXTHDD_MOUNTPOINT}/redis/data
REDIS_CONF=${EXTHDD_MOUNTPOINT}/redis/conf

mkdir -p ${REDIS_CONF} ${REDIS_DATA}

# stop/kill the current container
docker rm -f $(docker ps -a | grep ${IMAGE_NAME} | cut -f 1 -d " ")
#docker top $(docker ps | grep ${IMAGE_NAME}| cut -f 1 -d " ")

# Run the container
sudo docker run -d --restart=always -p 6379:6379 -v ${REDIS_CONF}:/usr/local/etc/redis -v ${REDIS_DATA}:/data ${DOCKER_TZ} --name ${IMAGE_NAME} --net-alias rasbr-${IMAGE_NAME} ${DOCKER_NETWORK} ${DOCKER_HOST} redis:latest redis-server /usr/local/etc/redis/redis.conf

#sudo docker run -d --restart=always -p 1880:1880 -v ${NODERED_DATA}:/data ${DOCKER_TZ} --name ${IMAGE_NAME} ${DOCKER_NETWORK} ${DOCKER_HOST} nodered/node-red:latest

# redis-cli test
# docker exec -it ef redis-cli
