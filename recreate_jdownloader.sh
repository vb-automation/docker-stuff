#!/bin/bash
# Include env variables
. docker.env
. crecentials

# Setup ENV variables
IMAGE_NAME="jdownloader"
#JD_USER=empty  #sourced from credentials file
#JD_PW=empty    #sourced from credentials file
JD_NAME=rasbr
JD_DOWN_PATH=${EXTHDD_MOUNTPOINT}/jd-down/downloads
JD_CONFIG_PATH=${EXTHDD_MOUNTPOINT}/jd-down/config

# make sure folders exist
mkdir -p ${JD_DOWN_PATH} ${JD_CONFIG_PATH}

# stop/kill the current container
docker rm -f $(docker ps | grep ${IMAGE_NAME} | cut -f 1 -d " ")

# Run the container
docker run -d --init --restart=always -v ${JD_DOWN_PATH}:/opt/JDownloader/Downloads -v ${JD_CONFIG_PATH}:/opt/JDownloader/app/cfg ${DOCKER_TZ} -u $(id -u) -p 3129:3129 -e MYJD_USER=${JD_USER} -e MYJD_PASSWORD=${JD_PW} -e MYJD_DEVICE_NAME=${JD_NAME} --name ${IMAGE_NAME} jaymoulin/jdownloader
